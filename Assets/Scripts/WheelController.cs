﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelController : MonoBehaviour
{
    float rotationalSpeed = 0f;

    bool spinning = false;

    Vector3 oldEulerAngles;

    void Start()
    {
        Application.targetFrameRate = 60;

        oldEulerAngles = gameObject.transform.rotation.eulerAngles;
        Debug.Log(oldEulerAngles);
    }

    void Update()
    {
        if (!spinning && Input.GetMouseButtonDown(0))
        {
            this.rotationalSpeed = 10f;
            this.spinning = true;
            GameController.instance.Spinning();
        }
        else if(spinning && this.rotationalSpeed == 0)
        {
            Debug.Log("Stopped");
            GameController.instance.DoneSpinning();
            this.spinning = false;
        } 

        transform.Rotate(0,0,this.rotationalSpeed);

        this.rotationalSpeed *= 0.9888f;
        if (this.rotationalSpeed <= 0.02f && this.rotationalSpeed > 0f)
        {
            this.rotationalSpeed = 0f;
            transform.Rotate(0,0,0);
        }
    }
}
