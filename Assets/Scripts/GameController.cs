﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    public GameObject playAgainText;

    // Start is called before the first frame update
    void Awake()
    {
        if (GameController.instance != null && GameController.instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            GameController.instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spinning()
    {
        playAgainText.SetActive(false);
    }

    public void DoneSpinning()
    {
        playAgainText.SetActive(true);
    }
}
